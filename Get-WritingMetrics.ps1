function Get-WritingMetrics {
    <#
    .SYNOPSIS
        A script to report on paragraph and sentence lengths
    .DESCRIPTION
        Takes input and generates several detailed views of the structure of the input document.
    .EXAMPLE
        PS C:\> "Test sentance? I think so! Time for some fun. Sentence 3.1 here? Nope!" | Get-WritingMetrics

            ParagraphCount SentenceCount WordCount
        -------------- ------------- ---------
                    1             5        13
        Name       SentenceCount Length SenNum1 SenNum2 SenNum3 SenNum4 SenNum5 Beginning
        ----       ------------- ------ ------- ------- ------- ------- ------- ---------
        Paragraph1             5     64 2/12    3/8     4/14    3/14    1/4     Test sentance? I think so! Time

    .EXAMPLE
        PS C:\> "Test sentance? I think so! Time for some fun.`n Sentence 3.1 here? Nope!" | Get-WritingMetrics -ExcludeDocumentSummary

            Name       SentenceCount Length SenNum1 SenNum2 SenNum3 Beginning
        ----       ------------- ------ ------- ------- ------- ---------
        Paragraph1             3     42 2/12    3/8     4/14    Test sentance? I think so! Time
        Paragraph2             2     22 3/14    1/4              Sentence 3.1 here? Nope!
    .INPUTS
        [string[]], [string]
    .OUTPUTS
        ConsoleHost, PSObject, Json, Csv
    .PARAMETER InputObject
    String or String array with at least `n between paragraphs
    .PARAMETER OutputType
    What output should be formatted as.
    .PARAMETER ExcludeSingleSentenceParagraphs
    When a "paragraph" only contains 1 sentence (like a header), skip it and remove from word count (reported under "RealWordCount")
    .PARAMETER ExcludeDocumentSummary
    By default, document summary is presented. This switch skips that.
    .PARAMETER SentenceCharacterLimit
    How many characters a sentence should be before marked as over in Out-Host output mode.
    .PARAMETER ParagraphCharacterLimit
    How many characters a paragraph should be before marked as over in Out-Host output mode.
    .PARAMETER Delimiter
    When set to output to Csv type, this param lets you customize the delimiter that you use.
    .LINK
    https://gitlab.com/devirich/writing-tools
    #>
    [cmdletbinding(DefaultParameterSetName = "All")]
    param (
        [parameter(ParameterSetName = "All", ValueFromPipeline)]
        [parameter(ParameterSetName = "Out-Host", ValueFromPipeline)]
        [parameter(ParameterSetName = "Csv", ValueFromPipeline)]
        $InputObject,

        [parameter(ParameterSetName = "All")]
        [parameter(ParameterSetName = "Out-Host")]
        [parameter(ParameterSetName = "Csv")]
        [ValidateSet("Out-Host", "PSObject", "Json", "Csv")]
        $OutputType = "Out-Host",

        [parameter(ParameterSetName = "All")]
        [parameter(ParameterSetName = "Out-Host")]
        [parameter(ParameterSetName = "Csv")]
        [switch]$ExcludeSingleSentenceParagraphs,

        [parameter(ParameterSetName = "All")]
        [parameter(ParameterSetName = "Out-Host")]
        [parameter(ParameterSetName = "Csv")]
        [switch]$ExcludeDocumentSummary,

        [parameter(ParameterSetName = "Out-Host")]
        $SentenceCharacterLimit = 75,

        [parameter(ParameterSetName = "Out-Host")]
        $ParagraphCharacterLimit = 300,

        [parameter(ParameterSetName = "Csv")]
        $Delimiter = ","


    )

    begin {
        $Document = [System.Collections.Generic.List[System.Object]]::New()
        $ParagraphSentenceMaxCount = 0
        $RealWordCount = 0
        $DocumentDetail = @()
        $DocumentSummary = [ordered]@{
            ParagraphCount = 0
            SentenceCount  = 0
            WordCount      = 0
        }
        $SentenceSplitRegex = "(?<=[.!?] )(?=[A-Z])"
        $NonWordSpaceRegex = "[^\w\s]"
    }
    process {
        foreach ($Paragraph in $InputObject -split "`n" | Where-Object { $_ }) {
            if ($ExcludeSingleSentenceParagraphs -and ($Paragraph -csplit $SentenceSplitRegex).Count -eq 1) { }
            else { $Document.Add($Paragraph) }
            $DocumentSummary.WordCount += ($Paragraph -replace $NonWordSpaceRegex -split " " | Where-Object { $_ }).Count
        }
    }
    end {
        $DocumentSummary["ParagraphCount"] = @($Document).Count

        $ParagraphCounter = 0
        foreach ($Paragraph in $Document) {
            $ParagraphCounter++
            $SentenceList = $Paragraph -csplit $SentenceSplitRegex | Where-Object { $_ }
            $ParagraphSummary = [ordered]@{
                Name          = "Paragraph$ParagraphCounter"
                SentenceCount = @($SentenceList).Count
                Beginning     = @($Paragraph)[0][0..30] -join ''
                Length        = ($Paragraph -replace $NonWordSpaceRegex).Length
            }

            $SentenceCounter = 0
            foreach ($Sentence in $SentenceList) {
                $SentenceCounter++
                $WordCount = @($Sentence -replace $NonWordSpaceRegex -split " " | Where-Object { $_ } ).Count
                $CharCount = ($Sentence -replace "\W").Length
                $ParagraphSummary["SenNum$SentenceCounter"] = "$WordCount/$CharCount"
                $RealWordCount += $WordCount
            }
            $DocumentSummary["SentenceCount"] += $SentenceCounter
            if ($SentenceCounter -gt $ParagraphSentenceMaxCount) { $ParagraphSentenceMaxCount = $SentenceCounter }
            $DocumentDetail += [PSCustomObject]$ParagraphSummary
        }

        if ($DocumentSummary["WordCount"] -ne $RealWordCount) { $DocumentSummary["RealWordCount"] = $RealWordCount }
        if ($ExcludeSingleSentenceParagraphs) { $DocumentSummary["RealWordCount"] = $RealWordCount }

        $Properties = "Name", "SentenceCount", "Length", (1..$ParagraphSentenceMaxCount | ForEach-Object { "SenNum$_" }), "Beginning" |
        ForEach-Object { $_ } # Need to unroll the incoming objects so Select-Object doesn't choke later
        switch ($OutputType) {
            "Out-host" {
                if (-not $ExcludeDocumentSummary) { [PSCustomObject]$DocumentSummary | Format-Table }

                $OriginalColor = [console]::ForegroundColor
                $OriginalColor = "White"
                Foreach ($ParagraphDetail in $DocumentDetail) {
                    if ($ParagraphCharacterLimit -and $ParagraphDetail.Length -gt $ParagraphCharacterLimit) {
                        $ParagraphDetail.Length = "~ {0}|" -f $ParagraphDetail.Length
                    }
                    foreach ($Sentence in $Properties | Where-Object { $_ -like "SenNum*" }) {
                        if ($SentenceCharacterLimit -and [int]($ParagraphDetail.$Sentence -replace ".*/") -gt $SentenceCharacterLimit) {
                            $ParagraphDetail.$Sentence = "~{0} |" -f $ParagraphDetail.$Sentence
                        }
                    }
                }
                ($DocumentDetail | Select-Object $Properties | Format-Table | out-string) -split "(?=~)|(?<=\|)" | ForEach-Object {
                    if ($_ -match "^~.*\|$") {
                        [console]::ForegroundColor = "red"
                        Write-Host -NoNewline ($_ -replace "^~ | \|$", "   " -replace "^~|\|$")
                        [console]::ForegroundColor = $OriginalColor
                    }
                    # elseif ($_.Trim() -eq "!@") { [console]::ForegroundColor = $OriginalColor }
                    else {
                        Write-Host -NoNewline $_
                    }
                }
                [console]::ForegroundColor = $OriginalColor
            }

            "PSObject" {
                if (-not $ExcludeDocumentSummary) { [PSCustomObject]$DocumentSummary | Format-Table | Out-Host }
                [PSCustomObject]$DocumentDetail | Select $Properties
            }

            "Json" {
                if ($ExcludeDocumentSummary) { $DocumentDetail | ConvertTo-Json -Depth 100 }
                else {
                    @{
                        DocumentSummary = $DocumentSummary
                        DocumentDetail  = $DocumentDetail
                    } | ConvertTo-Json -Depth 100
                }
            }

            "Csv" {
                if (-not $ExcludeDocumentSummary) { [PSCustomObject]$DocumentSummary | Format-Table | Out-Host }
                $DocumentDetail | Select-Object $Properties | ConvertTo-Csv -NoTypeInformation -Delimiter $Delimiter
            }
        }
    }
}

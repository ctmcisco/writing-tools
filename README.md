# Writing-tools

A collection of functions to help me write better: Metrics and more

## Get-WritingMetrics

A script to report on paragraph and sentence lengths

### Usage:

You must first capture some text to run the metrics against or pipe directly to the function

    "Test sentance? I think so! Time for some fun." | Get-WritingMetrics -OutputType Out-Host

Showcasing a few different switches:

    $Paragraphs = "# Heading1 - A paragraph that's not really a paragrah",
    "Intro sentence? I think so! The hardest part of sentences is keeping them short enough to be easily read and long enough to accurately convery their meaning.",
    "Second real paragraph. More text.

    A paragraph is defined as any strings seperated by newlines. This is the 3rd paragraph and they all contain periods and punctions, words, et. all. A sentence is seperated by a punctuation mark like !, ., or ? and is followed by a space and a capital letter."

Samples:

    PS> $Paragraphs | Get-WritingMetrics

![image](images/Get-WritingMetrics-1.png)

    PS> $Paragraphs | Get-WritingMetrics -ExcludeSingleSentenceParagraphs

![image](images/Get-WritingMetrics-2.png)

    PS> $Paragraphs |
        Get-WritingMetrics -ExcludeSingleSentenceParagraphs -ExcludeDocumentSummary -OutputType PSObject |
        Format-Table Name, Length, Beginning

![image](images/Get-WritingMetrics-3.png)

    PS> $Paragraphs |
        Get-WritingMetrics -ParagraphCharacterLimit 200 -SentenceCharacterLimit 100

![image](images/Get-WritingMetrics-4.png)

    PS> $Paragraphs |
        Get-WritingMetrics -OutputType Json -ExcludeSingleSentenceParagraphs -ExcludeDocumentSummary

![image](images/Get-WritingMetrics-5.png)
